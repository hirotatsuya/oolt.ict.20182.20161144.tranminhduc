
public class Aims {
	public static void main(String[] args) {
		
		final int MAX_LIMITED_ORDERS = 2;
		
		Order anOrder = new Order();
		// Create and add dvd 1 to Items Ordered List
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The lion king");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);

		// Create and add dvd 2 to Items Ordered List
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);

		// Create and add dvd 3 to Items Ordered List
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);
		
		// Create dvd 4
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Avatar");
		dvd4.setCategory("Science Fiction");
		dvd4.setCost(23.70f);
		dvd4.setDirector("James Cameron");
		dvd4.setLength(162);

		// Create dvd 5
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Kungfu Panda");
		dvd5.setCategory("Animation");
		dvd5.setCost(20.45f);
		dvd5.setDirector("Mark Osborne");
		dvd5.setLength(95);

		// Create dvd 6
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Aquaman");
		dvd6.setCategory("Science Fiction");
		dvd6.setCost(30.25f);
		dvd6.setDirector("James Wan");
		dvd6.setLength(142);

		// Total Cost
		System.out.print("\nTotal Cost is: ");
		System.out.println(anOrder.totalCost());
		

		// Remove items
		System.out.println("\n-TEST REMOVE FUNCTION-");
		anOrder.removeDigitalVideoDisc(dvd2);
		anOrder.shortPrintOrdered();
		

		// Add an array of items to ordered list
		System.out.println("\n-Test add an array of items to ordered list-");
		DigitalVideoDisc[] dvdList = { dvd4, dvd5 };
		anOrder.addDigitalVideoDisc(dvdList);
		anOrder.shortPrintOrdered();
		

		// Add an arbitrary number of items
		System.out.println("\n-Test add an arbitrary number of items-");
		anOrder.addDigitalVideoDisc(dvd3, dvd6, dvd1);
		anOrder.shortPrintOrdered();
		

		// Add 2 items
		System.out.println("\n-Test add 2 items-");
		anOrder.addDigitalVideoDisc(dvd2, dvd2);
		anOrder.shortPrintOrdered();
		
		
		// Classifier Member and Instance Member
		System.out.println("\n-TEST MAX_LIMITED_ORDERS-");
		
		Order order2;
	    if (Order.getNbOrders() >= MAX_LIMITED_ORDERS) {
	      System.out.println("Cannot create new order.");
	    } else {
	      order2 = new Order();
	      order2 = anOrder;
	      order2.printOrdered();
	    }

	    Order order3;
	    if (Order.getNbOrders() >= MAX_LIMITED_ORDERS) {
	      System.out.println("Cannot create new order. Orders list is full.");
	    } else {
	      order3 = new Order();
	      order3 = anOrder;
	      order3.printOrdered();
	    }
	    
	    Order order4;
	    if (Order.getNbOrders() >= MAX_LIMITED_ORDERS) {
	      System.out.println("Cannot create new order. Orders list is full.");
	    } else {
	      order4 = new Order();
	      order4 = anOrder;
	      order4.printOrdered();
	    }
	}

}
