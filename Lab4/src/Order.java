
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;
	private String dateOrdered;
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nbOrders = 0;

	public Order() {
		super();
		this.dateOrdered = "01 - 01 - 2019";
		nbOrders += 1;
	}

	// Print items ordered list
	public void printOrdered() {
		System.out.println("*********************Order************************");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered Items:");
		for (int i = 0; i < this.qtyOrdered; i++) {
			System.out.printf("%d. DVD - %s - %s - %s - %d: %f$\n", i + 1, itemsOrdered[i].getTitle(),
					itemsOrdered[i].getCategory(), itemsOrdered[i].getDirector(), itemsOrdered[i].getLength(),
					itemsOrdered[i].getCost());
		}
		System.out.println("Total cost: " + this.totalCost() + '$');
		System.out.println("**************************************************\n");
	}

	// Add 2 items
	public void addDigitalVideoDisc(DigitalVideoDisc disc1, DigitalVideoDisc disc2) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("Items ordered list is full.");
		} else if (this.qtyOrdered + 2 > MAX_NUMBERS_ORDERED) {
			System.out.println("Number of items that you want to add is too big. Cannot add.");
		} else {
			this.addDigitalVideoDisc(disc1);
			this.addDigitalVideoDisc(disc2);
		}
	}

	// Add an array of items to ordered list
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("Items ordered list is full.");
		} else if (this.qtyOrdered + dvdList.length > MAX_NUMBERS_ORDERED) {
			System.out.println("Number of items that you want to add is too big. Cannot add.");
		} else {
			for (int i = 0; i < dvdList.length; i++) {
				this.addDigitalVideoDisc(dvdList[i]);
			}
		}
	}

	// Add an arbitrary number of arguments
	public void addDigitalVideoDisc(DigitalVideoDisc disc, DigitalVideoDisc... dvdList) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("Items ordered list is full.");
		} else if (this.qtyOrdered + dvdList.length + 1 > MAX_NUMBERS_ORDERED) {
			System.out.println("Number of items that you want to add is too big. Cannot add.");
		} else {
			this.addDigitalVideoDisc(disc);
			this.addDigitalVideoDisc(dvdList);
		}
	}

	// Print Items Ordered List in short form
	public void shortPrintOrdered() {
		System.out.println("Items ordered list: ");
		for (int i = 0; i < this.qtyOrdered; i++) {
			System.out.println(this.itemsOrdered[i].getTitle());
		}
	}

	// Add item
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("Items ordered list is full.");
		} else {
			this.itemsOrdered[this.qtyOrdered] = disc;
			this.qtyOrdered++;
			System.out.println("Item " + disc.getTitle() + " is added.");
		}
	}

	// Remove Item
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int index;
		for (index = 0; index < qtyOrdered; index++) {
			if (this.itemsOrdered[index].getTitle() == disc.getTitle()) {
				break;
			}
		}

		DigitalVideoDisc[] array = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
		int k = 0;
		for (int i = 0; i < this.qtyOrdered; i++) {
			if (i == index) {
				continue;
			}

			array[k] = this.itemsOrdered[i];
			k++;
		}

		this.qtyOrdered -= 1;
		this.itemsOrdered = array;
		System.out.println("Item " + disc.getTitle() + " is removed.");
	}

	// Total Cost
	public float totalCost() {
		float totalCost = 0;
		for (int i = 0; i < this.qtyOrdered; i++) {
			totalCost += this.itemsOrdered[i].getCost();
		}
		return totalCost;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public static int getNbOrders() {
		return nbOrders;
	}

	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}

}