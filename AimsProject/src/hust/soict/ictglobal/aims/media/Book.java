package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media {
	private List<String> authors = new ArrayList<String>();

	public Book(String id) {
		super(id);
	}

	public Book(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}

	public Book(String id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		this.authors = authors;
	}

	// Add Authors
	public void addAuthor(String author) {
		if (isAuthor(author)) {
			System.out.println("Cannot add " + author + " .");
		} else {
			this.authors.add(author);
			System.out.println("Author " + author + " is added.");
		}
	}

	// Remove Authors
	public void removeAuthor(String author) {
		if (!isAuthor(author)) {
			System.out.println("Cannot remove " + author + " .");
		} else {
			this.authors.remove(author);
			System.out.println("Author " + author + " is removed.");
		}
	}

	// Check if author already existed
	private boolean isAuthor(String author) {
		for (String name : this.authors) {
			if (author.equals(name)) {
				return true;
			}
		}
		return false;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

}
