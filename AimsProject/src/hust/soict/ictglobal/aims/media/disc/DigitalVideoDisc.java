package hust.soict.ictglobal.aims.media.disc;

import hust.soict.ictglobal.aims.media.Playable;

public class DigitalVideoDisc extends Disc implements Playable {
	private String director;
	private int length;

	public DigitalVideoDisc(String id, String title, String category, float cost, int length, String director) {
		super(id, title, category, cost, length, director);

	}

	// Playable Interface
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}
