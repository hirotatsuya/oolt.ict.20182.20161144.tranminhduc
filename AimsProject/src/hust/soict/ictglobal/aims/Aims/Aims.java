package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.daemon.MemoryDaemon;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {
	public static void main(String[] args) {
		MemoryDaemon memoryDaemon = new MemoryDaemon();
	    Thread thread = new Thread(memoryDaemon);
	    thread.setDaemon(true);
	    thread.start();
	    
		int option;
		Scanner scanner = new Scanner(System.in);
		Order order = null;

		do {
			showMenu();
			option = scanner.nextInt();
			scanner.nextLine();
			System.out.println();
			switch (option) {

			// Create new order
			case 1: {
				System.out.print("Enter the order id: ");
				String id = scanner.nextLine();
				order = new Order(id);
				System.out.println("Order id " + id + " is created.");
				break;
			}

			// Add item to the order
			case 2: {
				if (order == null) {
					System.out.println("Cannot add item to the order. Please create order.");
					break;
				}
				order.getInputItem();
				break;
			}

			// Delete item by id
			case 3: {
				if (order == null) {
					System.out.println("Cannot delete item. Please create order.");
					break;
				}
				System.out.print("Enter the id: ");
				String id = scanner.nextLine();
				order.removeMedia(id);
				break;
			}

			// Display the items list of order
			case 4: {
				if (order == null) {
					System.out.println("Cannot display items. Please create order.");
					break;
				}
				order.printOrdered();
				break;
			}

			default:
				if (option != 0) {
					System.out.println("Please choose a number: 0-1-2-3-4.");
				}
			}
		} while (option != 0);
	}

	public static void showMenu() {
		System.out.println("\nOrder Management Application:");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.print("Please choose a number: 0-1-2-3-4: ");
	}
}
