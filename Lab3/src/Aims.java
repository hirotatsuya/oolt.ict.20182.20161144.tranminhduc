
public class Aims {

	public static void main(String[] args) {
		Order anOrder = new Order();

		// Create and add dvd 1 to Items Ordered List
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The lion king");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);

		// Create and add dvd 2 to Items Ordered List
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);
		
		// Create and add dvd 3 to Items Ordered List
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);

	
		// Total Cost
		System.out.print("\nTotal Cost is: ");
		System.out.println(anOrder.totalCost());

		// Remove items
		System.out.println("\n-TEST REMOVE FUNCTION-");
		anOrder.removeDigitalVideoDisc(dvd3);
		anOrder.printOrdered();
		System.out.print("Total Cost is: ");
		System.out.println(anOrder.totalCost());

	}

}
