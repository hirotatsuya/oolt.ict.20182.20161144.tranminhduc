
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;

	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;

	public Order() {
		super();
	}

	// Print items ordered list
	public void printOrdered() {
		System.out.println("Items ordered list: ");
		for (int i = 0; i < this.qtyOrdered; i++) {
			System.out.println(this.itemsOrdered[i].getTitle());
		}
	}

	// Add item
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (this.qtyOrdered == MAX_NUMBERS_ORDERED) {
			System.out.println("Items ordered list is full.");
		} else {
			this.itemsOrdered[this.qtyOrdered] = disc;
			this.qtyOrdered++;
			System.out.println("Item " + disc.getTitle() + " is added.");
		}
	}

	// Remove Item
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int index;
		for (index = 0; index < qtyOrdered; index++) {
			if (this.itemsOrdered[index].getTitle() == disc.getTitle()) {
				break;
			}
		}

		DigitalVideoDisc[] array = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
		int k = 0;
		for (int i = 0; i < this.qtyOrdered; i++) {
			if (i == index) {
				continue;
			}

			array[k] = this.itemsOrdered[i];
			k++;
		}

		this.qtyOrdered -= 1;
		this.itemsOrdered = array;
		System.out.println("Item " + disc.getTitle() + " is removed.");
	}

	// Total Cost
	public float totalCost() {
		float totalCost = 0;
		for (int i = 0; i < this.qtyOrdered; i++) {
			totalCost += this.itemsOrdered[i].getCost();
		}
		return totalCost;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

}