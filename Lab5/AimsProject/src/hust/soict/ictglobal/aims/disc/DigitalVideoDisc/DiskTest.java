package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.order.Order.Order;

public class DiskTest {

	public static void main(String[] args) {
		Order anOrder = new Order();
		// Create and add dvd 1 to Items Ordered List
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The lion king");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		anOrder.addDigitalVideoDisc(dvd1);

		// Create and add dvd 2 to Items Ordered List
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		anOrder.addDigitalVideoDisc(dvd2);

		// Create and add dvd 3 to Items Ordered List
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		anOrder.addDigitalVideoDisc(dvd3);

		// Create and add dvd 4 to Items Ordered List
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Avatar");
		dvd4.setCategory("Science Fiction");
		dvd4.setCost(23.70f);
		dvd4.setDirector("James Cameron");
		dvd4.setLength(162);
		anOrder.addDigitalVideoDisc(dvd4);

		// Create and add dvd 5 to Items Ordered List
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Kungfu Panda");
		dvd5.setCategory("Animation");
		dvd5.setCost(20.45f);
		dvd5.setDirector("Mark Osborne");
		dvd5.setLength(95);
		anOrder.addDigitalVideoDisc(dvd5);

		// Create and add dvd 6 to Items Ordered List
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Aquaman");
		dvd6.setCategory("Science Fiction");
		dvd6.setCost(30.25f);
		dvd6.setDirector("James Wan");
		dvd6.setLength(142);
		anOrder.addDigitalVideoDisc(dvd6);

		// Test search function
		System.out.println("\n-TEST SEARCH FUNCTION-\n");
		System.out.println("Test with dvd 5: " + dvd5.getTitle());
		System.out.println("Search 'pAnDa': " + dvd5.search("pAnDa"));
		System.out.println("Search 'Panda Kungfu': " + dvd5.search("Panda Kungfu"));
		System.out.println("Search 'Kungfu Tiger': " + dvd5.search("Kungfu Tiger"));
		System.out.println("Search 'Kungfu Panda': " + dvd5.search("Kungfu Panda"));
		System.out.println("Search 'kungfu panda': " + dvd5.search("kungfu panda"));
		System.out.println("Search 'Captain America': " + dvd5.search("Captain America"));

		// Test getAlucky Item()
		System.out.println("\n-TEST LUCKY ITEM-\n");
		DigitalVideoDisc luckyItem = anOrder.getALuckyItem();// get a lucky item
		System.out.println("Lucky Item is: " + luckyItem.getTitle() + "\n");// print lucky item title
		anOrder.printOrdered();
	}

}
