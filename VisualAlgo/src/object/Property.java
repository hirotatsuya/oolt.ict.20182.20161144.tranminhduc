package object;

public class Property {
	public static final int WIDTH = 25;
	public static final int HEIGHT = 7;
	public static final int DISTANCE = 35;
	public static final int RANDOM = 50;
	public static final int BUCKET_STACK = 10;
	public static final int SCENE_WIDTH = 1600;
	public static final int SCENE_HEIGHT = 900;
	public static final double DURATION = 0.2;
}
