package algorithm;

import object.ElementArray;

public class Heap {
	private static Heap init = new Heap();

	public static Heap getInit() {
		return init;
	}

	private Heap() {
	}

	public void sort(ElementArray array) {
		for (int i = (array.length() - 2) / 2; i >= 0; i--)
			heapify(array, i, array.length() - 1);

		for (int i = array.length() - 1; i > 0; i--) {
			array.swap(0, i);
			heapify(array, 0, i - 1);
		}
	}

	private void heapify(ElementArray array, int i, int m) {
		int j;
		while (2 * i + 1 <= m) {
			j = 2 * i + 1;
			if (j < m) {
				if (array.getElementAt(j).compareTo(array.getElementAt(j + 1)) > 0)
					j++;
			}
			if (array.getElementAt(i).compareTo(array.getElementAt(j)) > 0) {
				array.swap(i, j);
				i = j;
			} else
				i = m;
		}
	}

	public void swapKeys(int[] array, int i, int j) {
		int temp;
		temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
