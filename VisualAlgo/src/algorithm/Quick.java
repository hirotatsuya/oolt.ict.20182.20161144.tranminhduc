package algorithm;

import object.ElementArray;

public class Quick {
	private static Quick init = new Quick();

	public static Quick getInit() {
		return init;
	}

	private Quick() {
	}

	int partition(ElementArray arr, int low, int high) {
		int pivot = arr.getElementAt(high).getIndex();
		int i = (low - 1);
		for (int j = low; j < high; j++) {
			if (arr.compare(j, pivot) >= 0) {
				i++;

				arr.swap(i, j);
			}
		}
		arr.swap(i + 1, high);
		return i + 1;
	}

	public void sort(ElementArray arr, int low, int high) {
		if (low < high) {
			int pi = partition(arr, low, high);
			sort(arr, low, pi - 1);
			sort(arr, pi + 1, high);
		}
	}
}
