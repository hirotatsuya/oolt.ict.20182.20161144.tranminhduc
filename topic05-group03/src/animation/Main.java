package animation;

import algorithm.Bubble;
import algorithm.Heap;
import algorithm.Quick;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import object.ElementArray;

public class Main extends Application {

	Scene sceneMenu, sceneBubble, sceneHeap, sceneQuick, sceneRadix;

	@Override
	public void start(Stage primaryStage) {

		primaryStage.setTitle("VISUAL GO");

		// Button
		Button buttonBubble = new Button("BUBBLE SORT");
		Button buttonQuick = new Button("QUICK SORT");
		Button buttonHeap = new Button("HEAP SORT");
		Button buttonRadix = new Button("RADIX SORT");
		Button buttonExit = new Button("EXIT");

		// Set Style
		buttonBubble.setStyle("-fx-font: 30 arial;");
		buttonBubble.setLayoutX(650);
		buttonBubble.setLayoutY(300);
		buttonBubble.setPrefWidth(300);
		buttonBubble.setOnAction(e -> primaryStage.setScene(sceneBubble));

		buttonQuick.setStyle("-fx-font: 30 arial;");
		buttonQuick.setLayoutX(650);
		buttonQuick.setLayoutY(400);
		buttonQuick.setPrefWidth(300);
		buttonQuick.setOnAction(e -> primaryStage.setScene(sceneQuick));

		buttonHeap.setStyle("-fx-font: 30 arial;");
		buttonHeap.setLayoutX(650);
		buttonHeap.setLayoutY(500);
		buttonHeap.setPrefWidth(300);
		buttonHeap.setOnAction(e -> primaryStage.setScene(sceneHeap));

		buttonRadix.setStyle("-fx-font: 30 arial;");
		buttonRadix.setLayoutX(650);
		buttonRadix.setLayoutY(600);
		buttonRadix.setPrefWidth(300);
		buttonRadix.setOnAction(e -> primaryStage.setScene(sceneRadix));

		buttonExit.setStyle("-fx-font: 30 arial;");
		buttonExit.setLayoutX(650);
		buttonExit.setLayoutY(700);
		buttonExit.setPrefWidth(300);
		buttonExit.setOnAction(e -> Platform.exit());

		// Setting the text
		Text text1 = new Text("VISU");
		text1.setFont(Font.font(null, FontWeight.BOLD, 100));
		text1.setFill(Color.BLACK);
		text1.setX(570);
		text1.setY(200);

		Text text2 = new Text("ALGO");
		text2.setFont(Font.font(null, FontWeight.BOLD, 100));
		text2.setFill(Color.AQUA);
		text2.setX(800);
		text2.setY(200);

		// Menu Scene
		AnchorPane menuPane = new AnchorPane();
		menuPane.setPrefSize(600, 900);
		menuPane.setStyle("-fx-background-color:  #fffde1;");

		// Set up
		menuPane.getChildren().addAll(buttonBubble, buttonQuick, buttonHeap, buttonRadix, buttonExit);
		menuPane.getChildren().addAll(text1, text2);
		sceneMenu = new Scene(menuPane, 1600, 900);

		// Bubble Scene
		// Array
		ElementArray arrayBubble = new ElementArray(35);

		// Anchor Pane
		AnchorPane bubblePane = new AnchorPane();
		bubblePane.setPrefSize(1600, 900);
		bubblePane.setStyle("-fx-background-color:  #fffde1;");

		// Get node
		bubblePane.getChildren().addAll(arrayBubble.getAll());

		// Set up
		Bubble.getInit().sort(arrayBubble);
		arrayBubble.getAnimation().play();
		sceneBubble = new Scene(bubblePane, 1600, 900);

		// Heap Scene
		// Array
		ElementArray arrayHeap = new ElementArray(35);

		// Anchor Pane
		AnchorPane heapPane = new AnchorPane();
		heapPane.setPrefSize(1600, 900);
		heapPane.setStyle("-fx-background-color:  #fffde1;");

		// Get node
		heapPane.getChildren().addAll(arrayHeap.getAll());

		// Set up
		Heap.getInit().sort(arrayHeap);
		arrayHeap.getAnimation().play();
		sceneHeap = new Scene(heapPane, 1600, 900);

		// Quick Scene
		// Array
		ElementArray arrayQuick = new ElementArray(35);

		// Anchor Pane
		AnchorPane quickPane = new AnchorPane();
		quickPane.setPrefSize(1600, 900);
		quickPane.setStyle("-fx-background-color:  #fffde1;");

		// Get node
		quickPane.getChildren().addAll(arrayQuick.getAll());

		// Set up
		Quick.getInit().sort(arrayQuick, 0, arrayQuick.length() - 1);
		arrayQuick.getAnimation().play();
		sceneQuick = new Scene(quickPane, 1600, 900);

		primaryStage.setScene(sceneMenu);
		primaryStage.setResizable(false);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}
}
