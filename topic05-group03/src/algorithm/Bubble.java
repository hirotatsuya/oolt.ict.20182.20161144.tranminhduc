package algorithm;

import object.ElementArray;

public class Bubble {
	private static Bubble init = new Bubble();

	public static Bubble getInit() {
		return init;
	}

	private Bubble() {
	}

	public void sort(ElementArray array) {

		for (int i = 0; i < array.length() - 1; i++) {
			for (int j = 0; j < array.length() - i - 1; j++) {
				array.compareAndSwap(j, j + 1);
			}
		}
	}
}
