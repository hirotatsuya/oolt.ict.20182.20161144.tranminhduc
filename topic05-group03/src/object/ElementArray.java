package object;

import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class ElementArray {
	private Element[] elements;

	private SequentialTransition animation;

	public ElementArray(int length) {
		elements = new Element[length];
		animation = new SequentialTransition();

		for (int i = 0; i < length; i++) {
			int value = (int) (Math.random() * Property.RANDOM) + 1;
			elements[i] = new Element(value);
			elements[i].setIndex(i);

			elements[i].setX(Property.SCENE_WIDTH / 2 - Property.DISTANCE * length / 2 + i * Property.DISTANCE);
			elements[i].setY(Property.SCENE_HEIGHT * 0.7 - value * Property.HEIGHT);
			elements[i].setFill(Color.AQUA);
		}
	}

	public Element[] getAll() {
		return elements;
	}

	public Element getElementAt(int i) {
		return elements[i];
	}

	public int length() {
		return elements.length;
	}

	public void swap(int i, int j) {

		Element tmp = elements[i];
		elements[i] = elements[j];
		elements[j] = tmp;

		elements[i].setIndex(i);
		elements[j].setIndex(j);

		TranslateTransition tt1 = new TranslateTransition();
		tt1.setDuration(Duration.seconds(Property.DURATION));
		tt1.setByX(Property.DISTANCE * (i - j));
		tt1.setNode(elements[i]);

		TranslateTransition tt2 = new TranslateTransition();
		tt2.setDuration(Duration.seconds(Property.DURATION));
		tt2.setByX(Property.DISTANCE * (j - i));
		tt2.setNode(elements[j]);

		ParallelTransition pt = new ParallelTransition();
		pt.getChildren().addAll(tt1, tt2);

		animation.getChildren().add(pt);
	}

	public boolean compareAndSwap(int i, int j) {
		if (elements[i].compareTo(elements[j]) < 0) {
			swap(i, j);
			return true;
		}
		return false;
	}

	public SequentialTransition getAnimation() {
		return animation;
	}

	public int compare(int i, int j) {
		return elements[i].compareTo(elements[j]);
	}

	public Element getMax() {
		Element max = elements[0];
		for (Element e : elements) {
			if (max.compareTo(e) > 0) {
				max = e;
			}
		}
		return max;
	}

	public void moveY(int i, int y) {
		TranslateTransition tt1 = new TranslateTransition();
		tt1.setDuration(Duration.seconds(Property.DURATION));
		tt1.setByY(-Property.BUCKET_STACK * y);
		tt1.setNode(elements[i]);

		animation.getChildren().add(tt1);
	}
}