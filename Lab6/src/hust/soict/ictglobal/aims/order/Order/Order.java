package hust.soict.ictglobal.aims.order.Order;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private static Scanner scanner = new Scanner(System.in);
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private String id;
	private String dateOrdered;

	public Order(String id) {
		this.id = id;
		this.dateOrdered = "01 - 01 - 2019";
	}

	// Add media
	public void addMedia(Media item) {
		if (this.itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
			System.out.println("Cannot add" + item.getTitle() + ". Item ordered list is full.");
		} else {
			this.itemsOrdered.add(item);
			System.out.println(item.getTitle() + " is added.");
		}
	}

	// Get Input Item
	public void getInputItem() {
		System.out.println("Select the type of item:");
		System.out.println("1. DVD");
		System.out.println("2. Book");
		System.out.print("Enter your choice: 1 - 2: ");

		int choice = scanner.nextInt();
		scanner.nextLine();

		if (choice != 1 && choice != 2) {
			System.out.println("Please enter 1 or 2.");
			return;
		}

		System.out.print("Enter the id: ");
		String id = scanner.nextLine();

		System.out.print("Enter the title: ");
		String title = scanner.nextLine();

		System.out.print("Enter the category: ");
		String category = scanner.nextLine();

		System.out.print("Enter the cost: ");
		float cost = scanner.nextFloat();
		scanner.nextLine();

		if (choice == 1) {
			System.out.print("Enter the director name: ");
			String director = scanner.nextLine();

			System.out.print("Enter the length of DVD: ");
			int length = scanner.nextInt();
			scanner.nextLine();

			DigitalVideoDisc dvd = new DigitalVideoDisc(id, title, category, cost, director, length);
			addMedia(dvd);
			return;
		} else {
			Book book = new Book(id, title, category, cost);
			System.out.print("Enter the number of authors: ");
			int numberOfAuthor = scanner.nextInt();
			scanner.nextLine();

			for (int i = 0; i < numberOfAuthor; i++) {
				System.out.print("Enter the author name: ");
				String author = scanner.nextLine();
				book.addAuthor(author);
			}
			addMedia(book);
			return;
		}
	}

	// Remove media
	public void removeMedia(String id) {
		if (this.itemsOrdered.size() == 0) {
			System.out.println("Cannot remove. Item ordered list is empty.");
			return;
		} else {
			for (Media item : itemsOrdered) {
				if (item.getId().equals(id)) {
					this.itemsOrdered.remove(item);
					System.out.println(item.getTitle() + " is removed.");
					return;
				}
			}
			System.out.println("Item id " + id + " does not exist.");
		}
	}

	// Total cost
	public float totalCost() {
		float totalCost = 0;
		for (Media item : this.itemsOrdered) {
			totalCost += item.getCost();
		}
		return totalCost;
	}

	// Print items ordered list
	public void printOrdered() {
		System.out.println("****************Order*******************");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered items:");
		for (Media item : itemsOrdered) {
			item.printMedia();
		}
		System.out.println("Total cost: " + this.totalCost() + '$');
		System.out.println("****************************************\n");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

}