package hust.soict.ictglobal.aims.media;

public class Media {
	private String title;
	private String category;
	private String id;
	private float cost;

	public Media(String id) {
		super();
		this.id = id;
	}

	public Media(String id, String title, String category, float cost) {
		super();
		this.id = id;
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	//Print informations of the item
	public void printMedia() {
		System.out.println(this.getId() + " - " + this.getTitle() + " - " + this.getCategory() + ": " + this.getCost());
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
