package hust.soict.ictglobal.aims.order.Order;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.media.disc.CompactDisc;
import hust.soict.ictglobal.aims.media.disc.DigitalVideoDisc;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private static Scanner scanner = new Scanner(System.in);
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private String id;
	private String dateOrdered;

	public Order(String id) {
		this.id = id;
		this.dateOrdered = "01 - 01 - 2019";
	}

	// Add media
	public void addMedia(Media item) {
		if (this.itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
			System.out.println("Cannot add" + item.getTitle() + ". Item ordered list is full.");
		} else {
			this.itemsOrdered.add(item);
			System.out.println(item.getTitle() + " is added.");
		}
	}

	// Get Input Item
	public void getInputItem() {
		System.out.println("Select type:");
		System.out.println("1. Book");
		System.out.println("2. CD");
		System.out.println("3. DVD");
		System.out.print("Enter your choice: 1 - 2 - 3: ");

		int choice = scanner.nextInt();
		scanner.nextLine();

		if (choice < 1 || choice > 3) {
			System.out.println("Please enter 1 or 2 or 3.");
			return;
		}

		System.out.print("Enter the id: ");
		String id = scanner.nextLine();

		System.out.print("Enter the title: ");
		String title = scanner.nextLine();

		System.out.print("Enter the category: ");
		String category = scanner.nextLine();

		System.out.print("Enter the cost: ");
		float cost = scanner.nextFloat();
		scanner.nextLine();

		if (choice == 1) {
			Book book = new Book(id, title, category, cost);
			System.out.print("Enter the number of authors: ");
			int numberOfAuthor = scanner.nextInt();
			scanner.nextLine();

			for (int i = 0; i < numberOfAuthor; i++) {
				System.out.print("Enter authors number " + (i + 1) + ": ");
				String author = scanner.nextLine();
				book.addAuthor(author);
			}
			addMedia(book);
			return;
		} else if (choice == 2) {
			System.out.print("Enter the director: ");
			String director = scanner.nextLine();

			System.out.print("Enter the artist: ");
			String artist = scanner.nextLine();

			CompactDisc cd = new CompactDisc(id, title, category, cost, 0, director, artist);

			System.out.print("Enter the number of tracks: ");
			int numberOfTrack = scanner.nextInt();
			scanner.nextLine();

			for (int i = 0; i < numberOfTrack; i++) {
				System.out.print("Enter title of track number " + (i + 1) + ": ");
				String trackTitle = scanner.nextLine();

				System.out.print("Enter length of track number " + (i + 1) + ": ");
				int trackLength = scanner.nextInt();
				scanner.nextLine();

				Track track = new Track(trackTitle, trackLength);
				cd.addTrack(track);
			}
			cd.setLength(cd.getLength());
			addMedia(cd);

			System.out.println("Do you want to play this CD?");
			System.out.println("Enter 1 for YES. Enter others numbers for No.");
			System.out.print("Your choice: ");
			int isPlay = scanner.nextInt();
			scanner.nextLine();

			if (isPlay == 1) {
				cd.play();
			} else {
				return;
			}

		} else {
			System.out.print("Enter the director: ");
			String director = scanner.nextLine();

			System.out.print("Enter the length: ");
			int length = scanner.nextInt();
			scanner.nextLine();

			DigitalVideoDisc dvd = new DigitalVideoDisc(id, title, category, cost, length, director);

			addMedia(dvd);

			System.out.println("Do you want to play this DVD?");
			System.out.println("Enter 1 for YES. Enter others numbers for No.");
			System.out.print("Your choice: ");
			int isPlay = scanner.nextInt();
			scanner.nextLine();

			if (isPlay == 1) {
				dvd.play();
			} else {
				return;
			}
		}
	}

	// Remove media
	public void removeMedia(String id) {
		if (this.itemsOrdered.size() == 0) {
			System.out.println("Cannot remove. Item ordered list is empty.");
			return;
		} else {
			for (Media item : itemsOrdered) {
				if (item.getId().equals(id)) {
					this.itemsOrdered.remove(item);
					System.out.println(item.getTitle() + " is removed.");
					return;
				}
			}
			System.out.println("Item id " + id + " does not exist.");
		}
	}

	// Total cost
	public float totalCost() {
		float totalCost = 0;
		for (Media item : this.itemsOrdered) {
			totalCost += item.getCost();
		}
		return totalCost;
	}

	// Print items ordered list
	public void printOrdered() {
		System.out.println("****************Order*******************");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered items:");
		for (Media item : itemsOrdered) {
			item.printMedia();
		}
		System.out.println("Total cost: " + this.totalCost() + '$');
		System.out.println("****************************************\n");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

}