package hust.soict.ictglobal.aims.media.disc;

import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.Playable;
import hust.soict.ictglobal.aims.media.Track;

public class CompactDisc extends Disc implements Playable {
	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public CompactDisc(String id, String title, String category, float cost, int length, String director,
			String artist) {
		super(id, title, category, cost, length, director);
		this.artist = artist;
	}

	// Add Track
	public void addTrack(Track track) {
		if (isTrack(track)) {
			System.out.println(track.getTitle() + " is already in the list of tracks.");
		} else {
			this.tracks.add(track);
			System.out.println(track.getTitle() + " is added.");
		}
	}

	// Remove Track
	public void removeTrack(Track track) {
		if (!isTrack(track)) {
			System.out.println(track.getTitle() + " is not in the list of tracks.");
		} else {
			this.tracks.remove(track);
			System.out.println(track.getTitle() + " is removed.");
		}
	}

	// Check whether track is already in the list of tracks
	private boolean isTrack(Track track) {
		for (Track oneTrack : this.tracks) {
			if (oneTrack.getTitle().equals(track.getTitle())) {
				return true;
			}
		}
		return false;
	}

	// Get length
	public int getLength() {
		int totalLength = 0;
		for (Track oneTrack : this.tracks) {
			totalLength += oneTrack.getLength();
		}
		return totalLength;
	}

	// Playable Interface
	public void play() {
		System.out.println("Artist: " + this.getArtist());
		System.out.println("CD length: " + this.getLength());
		for (Track oneTrack : this.tracks) {
			oneTrack.play();
		}
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

}
